const { pool } = require("../database");

const getAuth = async username => {
  const text = "SELECT * FROM users WHERE username = $1";
  const value = [username];

  try {
    const data = await pool.query(text, value);
    return data.rows[0];
  } catch (err) {
    throw err;
  }
};

const getUser = async id => {
  const text = "SELECT * FROM users WHERE id=$1";
  const value = [id];

  try {
    const data = await pool.query(text, value);
    return data.rows[0];
  } catch (err) {
    throw err;
  }
};

const checkUsername = async username => {
  const text = "SELECT * FROM users WHERE username=$1";
  const value = [username];

  try {
    const data = await pool.query(text, value);
    return data.rows[0];
  } catch (err) {
    throw err;
  }
};

const createUser = async (username, hashPassword, fullname) => {
  const text =
    "INSERT INTO users (username, password, name, role) VALUES($1, $2, $3, $4) RETURNING *";
  const value = [username, hashPassword, fullname, 'user'];

  try {
    const data = await pool.query(text, value);
    return data.rows[0];
  } catch (err) {
    throw err;
  }
};

const getAllUsers = async (page, size) => {
  const text = "SELECT * FROM users ORDER BY id LIMIT $2 OFFSET (($1 - 1) * $2)";
  const values = [page, size];
  try {
    // statements
    const data = await pool.query(text, values);
    const {rows} = data
    return {
      data: rows,
      page: page,
      size: size  
    };
  } catch(e) {
    // statements
    console.error(e);
    throw e;
  }
}

module.exports = {
  getAuth,
  getUser,
  checkUsername,
  createUser,
  getAllUsers,
};
