const router = require("express").Router();
const { passwordIsMatch, getLoggedIn, createNewUser } = require("./service");
const auth = require("./auth");
const repo = require("./repository");
const {body, validationResult} = require('express-validator');

// @route      GET /auth
// @desc       Get logged in user
// @access     Private
router.get("/", auth(), async (req, res) => {
  let result = await getLoggedIn(req.user.id);
  res.json(result);
});

// @route      POST /login
// @desc       Auth user & get token
// @access     Public
router.post("/login", [
  body('username')
    .trim()
    .isLength({min: 2})
    .withMessage('Username must be at least 2 characters')
    .notEmpty()
    .withMessage('Username is required'),
  body('password')
    .trim()
    .isLength({min: 5})
    .withMessage('Password must be at least 5 characters')
    .notEmpty()
    .withMessage('Password is required')
],async (req, res) => {
  const { username, password } = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()){
    return res.status(400).json({ errors: errors.array() })
  }

  let result = await passwordIsMatch(username, password);
  res.json(result);
});

// @route      POST /signup
// @desc       Register New User
// @access     Private and only Admin role
router.post("/signup", [
  body('username')
    .trim()
    .isLength({min: 2})
    .withMessage('Username must be at least 2 characters')
    .notEmpty()
    .withMessage('Username is required'),
  body('password')
    .trim()
    .isLength({min: 5})
    .withMessage('Password must be at least 5 characters')
    .notEmpty()
    .withMessage('Password is required')
], auth(['admin','user']), async (req, res) => {
  const createdId = req.user.id;
  const { fullname, username, password } = req.body;

  const errors = validationResult(req);
  if (!errors.isEmpty()){
    return res.status(400).json({ errors: errors.array() })
  }

  let result = await createNewUser(createdId, username, password, fullname);
  res.json(result);
});

// @route      GET /auth/list
// @desc       Get All User as List
// @access     
router.get("/list", auth(['admin','user']),async (req, res) => {
  const { page, size } = req.query;
  
  let result = await repo.getAllUsers(page, size);
  res.json(result);
})

module.exports = router;
