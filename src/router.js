const auth = require("./core/auth/auth");

const registerRoute = (app) => {
    app.get("/api/safe", async (req, res) => {
        return res.json({ msg: "safe" });
    });

    app.use("/auth", require("./core/auth/controller"));
};

module.exports = {
    registerRoute,
};
